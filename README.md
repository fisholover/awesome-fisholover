# Awesome Fisholover

## Table of Contents
- [Stock Photos](#stock-photos)
- [Image Manipulation](#image-manipulation)

## Stock Photos
- [RGB Stock](http://www.rgbstock.com/)
- [Fotolia](https://www.fotolia.com/)

## Image Manipulation
- [Shutterstock Editor](https://www.shutterstock.com/editor)
- [Resizeimage](http://resizeimage.net/)
- [Croppola](https://croppola.com/)
- [Image Resize Calculator](https://red-route.org/code/image-resizing-calculator)
